<?php

$token_mismatch = false;

$sessionValidity = 3600 * 24 * 3; // one year

$cfg['LoginCookieValidity'] = $sessionValidity;
ini_set('session.gc_maxlifetime', $sessionValidity);
