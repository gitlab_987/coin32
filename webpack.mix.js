const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// need for pug
mix.webpackConfig({
   module: {
      rules: [
         {
            test: /\.pug$/,
            oneOf: [
               {
                  resourceQuery: /^\?vue/,
                  use: ['pug-plain-loader']
               },
               {
                  use: ['raw-loader', 'pug-plain-loader']
               }
            ]
         }
      ]
   },
  watchOptions: {
    aggregateTimeout: 2000,
    poll: 2000,
    ignored: /node_modules/
  }
});

mix.js('resources/js/app.js', 'app/public/js')
    .sass('resources/sass/app.scss', 'app/public/css');
