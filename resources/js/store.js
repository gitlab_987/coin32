import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex)

axios.defaults.baseURL = 'http://127.0.0.1:80'; // почему-то вызывает options
axios.defaults.withCredentials = true


export default new Vuex.Store({
    state: {
    },
    actions: {
        // save url id data using api
        saveUrlById({ commit }, payload) {
            return new Promise((resolve, reject) => {
                axios.put(`api/url/${payload.id}/`, {
                    "origin": payload.origin,
                    "slug": payload.slug
                }, {
                    headers: {
                        "X-CSRFToken": payload.csrftoken,
                    }
                }).then(response => {
                    resolve({
                    })
                    console.log('created')
                }).catch(error => {
                    reject({
                        error: error,
                        data: [error.response.data]
                    })
                    console.log(error)
                })
            });

        },
        // get url data by id using api
        getUrlById({ commit }, payload) {
            return new Promise((resolve, reject) => {
                axios.get(`api/url/${payload.id}/`, {}
                ).then(response => {
                    resolve({
                        id: response.data.id,
                        origin: response.data.origin,
                        slug: response.data.slug,
                    })
                    console.log('loaded url id')
                }).catch(error => {
                    console.log('loaded url error', error)
                    reject(error)
                })
            })
        },
        // create url record using api
        createUrl({ commit }, payload) {
            return new Promise((resolve, reject) => {
                axios.post(`api/url/`, {
                    // params: { 

                    // },
                    "origin": payload.origin
                }, {
                    headers: {
                        "X-CSRFToken": payload.csrftoken,
                    }
                }).then(response => {
                    resolve({

                    })
                    console.log('created')
                }).catch(error => {
                    reject({
                        error: error,
                        data: [error.response.data]
                    })
                    console.log(error)
                })
            });
        },
        // delete url by id using api
        deleteUrl({ commit }, payload) {
            console.log(payload.csrftoken);
            return new Promise((resolve, reject) => {
                axios.delete(`api/url/${payload.id}/`, {
                    // params: {},
                    headers: {
                        "X-CSRFToken": payload.csrftoken,
                    },
                }).then(response => {
                    resolve({
                        'success': response.status == 204
                    })
                }
                ).catch(error => {
                    console.log(error)
                    reject(error)
                })
            })
        },
        // get list of url page by page using api
        getTable({ commit }, payload) {
            return new Promise((resolve, reject) => {
                axios.get("api/url/", {
                    params: {
                        page: payload.page,
                    },
                })
                    .then(response => {
                        resolve({
                            'count': response.data.count,
                            'items': response.data.results,
                            'next': response.data.next,
                            'previous': response.data.previous,
                        })
                        console.log('!!!')
                        console.log(response.data.count)
                    })
                    .catch(error => {
                        console.log(error);
                        reject(error);
                    })
            });
        },
    },
})