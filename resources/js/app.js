import Vue from 'vue'
import VueCookies from 'vue-cookies';
import store from './store.js'
import axios from 'axios'
import App from './App.vue'

Vue.use(VueCookies);

window.Vue = require('vue')

new Vue({
  delimiters: ["[[", "]]"],
  el: '#app',
  store,
  component: { App },
  render: h => h(App),
}).$mount('#app')
