# Сервис сокращения ссылок

Тестовое задание разработчика

задание:
https://docs.google.com/document/d/1La9LOrj8qdpt4CVJOYAhuIrK0L0SOVIxfYp4CC5yU3A/edit

- интерфейс написан на Vue.js
- бекенд на django/ django rest framework
- задачи по расписанию на celery


каждые 10 сек удаляются все сообщения старше 10 минут
регулируется settings.URL_TTL = 10



### Api Documentatuon

http://127.0.0.1/swagger-ui/

http://127.0.0.1/openapi

 

### Getting started
just start 

    docker-compose up --build

  

### For debuging
http://127.0.0.1:5050 - pgAdmin

http://127.0.0.1:8888 - Flower

http://127.0.0.1/admin

все логины пароли в .env.dev

to enter admin panel need to create admin user

    docker exec -it <app_name>_app_1 python manage.py createsuperuser


