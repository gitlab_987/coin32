from django.urls import path, include
from rest_framework import routers
from .api import UrlViewSet
from .views import index, shorten_redis


app_name = "short"

router = routers.DefaultRouter()
router.register(r"url", UrlViewSet)


urlpatterns = [
    path(r"", index),
    path("api/", include(router.urls), name="url"),
    path("<slug:slug>", shorten_redis),
]
