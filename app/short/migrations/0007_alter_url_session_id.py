# Generated by Django 3.2.11 on 2022-01-31 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('short', '0006_auto_20220131_2037'),
    ]

    operations = [
        migrations.AlterField(
            model_name='url',
            name='session_id',
            field=models.CharField(max_length=32),
        ),
    ]
