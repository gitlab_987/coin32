# from datetime import datetime
from django.db import models
from django.core.validators import URLValidator, RegexValidator
from django.utils import timezone
from django.conf import settings
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
import logging
from short.redis_url import RedisUrl



# Create your models here.


def get_random_slug():
    """generate random slug and check its existance"""

    from random import randint

    az = [chr(ord("a") + a) for a in range(26)]
    AZ = [chr(ord("A") + a) for a in range(26)]
    zero_nine = [chr(ord("0") + a) for a in range(10)]
    alphabet = "".join(az + AZ + zero_nine)

    len_ = 62  #  len(alphabet)
    # infinit cycle until slug is unique
    while True:
        slug = "".join(([alphabet[randint(0, len_ - 1)] for i in range(10)]))
        try:
            Url.objects.get(slug=slug)
        except Url.DoesNotExist as err:
            break
    return slug


class Url(models.Model):

    origin = models.CharField(max_length=2048, validators=[URLValidator()])
    slug = models.CharField(
        max_length=10,
        default=get_random_slug,
        unique=True,
        validators=[
            RegexValidator(
                regex="^[a-zA-Z0-9]+$",
                message="Slug must be Alphanumeric case insensitive",
            ),
        ],
    )
    session_id = models.CharField(max_length=32, blank=False)  # добавить валидатор
    сreated = models.DateTimeField(auto_now_add=True, blank=True)


    class Meta:
        db_table = "url"


@receiver(post_save, sender=Url)
def save_to_redis(sender, instance, **kwargs):
    """save/update redis url if mysql was updated"""

    r_url = RedisUrl.get_by_id(int(instance.id))

    if r_url is not None:
        # url exists in redis
        # delete first
        r_url.delete()
    # create new url
    r_url = RedisUrl(instance.slug, instance.origin, instance.id)

    return r_url.save()


@receiver(post_delete, sender=Url)
def delete_from_redis(sender, instance, **kwargs):
    """delete redis url if mysql url was deleted"""

    r_url = RedisUrl.get_by_id(int(instance.id))
    print(r_url)
    if r_url is not None:
        return r_url.delete()
