from django.contrib import admin
from django.contrib.auth.models import User, Group


from .models import Url

# Register your models here.


admin.site.unregister(User)
admin.site.unregister(Group)


@admin.register(Url)
class UrlAdmin(admin.ModelAdmin):
    list_display = ("id", "origin", "slug", "session_id", "сreated")
