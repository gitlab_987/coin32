from django.test import TestCase, Client
from rest_framework.test import APITestCase
from rest_framework import status

from .models import Url

# Create your tests here.

client = Client()


class UrlTests(APITestCase):
    """testing Url api"""

    def test_create(self):

        response = client.post(
            "/api/url/",
            data={"origin": "http://evernote.com"},
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(Url.objects.get(origin="http://evernote.com"))

    def test_list(self):

        for url in ["http://google.com", "http://evernote.com"]:
            client.post(
                "/api/url/",
                data={"origin": url},
                content_type="application/json",
            )

        response = client.get(
            "/api/url/",
            content_type="application/json",
        )
        self.assertEqual(
            tuple(response.json()["results"][0].keys()) == ("id", "origin", "slug"),
            True,
        )

        self.assertEqual(len(response.json()["results"]), 2)
