from __future__ import annotations
import redis
import json
from django.conf import settings


redis_instance = redis.StrictRedis(
    host=settings.REDIS_HOST,
    port=settings.REDIS_PORT,
    db=0,
    charset="utf-8",
    decode_responses=True,
)


class RedisUrl:
    """redis Url object"""

    def __init__(self, slug: str, origin: str, id: int) -> None:
        self.slug = slug
        self.origin = origin
        self.id = id

    @classmethod
    def _get_by_mask(cls, mask: str) -> RedisUrl:
        """get url object by key mask from redis"""

        keys = redis_instance.keys(mask)
        if len(keys) == 0:
            return None
        else:
            # get key and get value of key
            key = keys[0]
            res = redis_instance.get(key)
            res_dict = json.loads(res)
            slug = key.split(":")[1]
            redis_url = RedisUrl(
                slug=slug, origin=res_dict["origin"], id=res_dict["id"]
            )
            return redis_url

    @classmethod
    def get_by_slug(cls, slug: str) -> RedisUrl:
        return RedisUrl._get_by_mask(f"url:{slug}:*")

    @classmethod
    def get_by_id(cls, id: int) -> RedisUrl:
        return RedisUrl._get_by_mask(f"url:*:{id}")

    def get_key(self) -> str:
        """generate redis key"""
        return f"url:{self.slug}:{self.id}"

    def save(self):
        """save new url to redis"""

        return redis_instance.set(
            self.get_key(), json.dumps(dict(origin=self.origin, id=self.id))
        )

    def delete(self):
        """delete redis url object"""

        return redis_instance.delete(self.get_key())

    def __repr__(self) -> str:
        return f"RedisUrl({self.slug}, {self.origin}, {self.id})"
