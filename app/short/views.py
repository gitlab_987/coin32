from cgitb import reset
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.views.decorators.http import require_GET
from .models import Url
from .redis_url import RedisUrl


# Create your views here.


def index(request):
    return render(request, "index.html")


@require_GET
def shorten_redis(request, slug):
    r_url = RedisUrl.get_by_slug(slug)
    if r_url is not None:
        return redirect(r_url.origin)
    else:
        raise Http404("Nothing found")



def page_not_found(request, exception):
    """
    404 Handler
    to work correctly DEBUG=False
    """
    response = render(request, "404.html", {})
    response.status_code = 404
    return response
