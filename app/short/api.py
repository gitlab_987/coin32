from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from .serializers import UrlSerializer
from .models import Url

from django.conf import settings
import logging




def set_session(fn):
    """
    decorator uses to turn on session if not available and
    sends session coockies in response
    """

    def _check(self, request):
        if not request.session.exists(request.session.session_key):
            request.session.create()
        res = fn(self, request)
        res.set_cookie("sessionid", request.session.session_key)
        return res

    return _check


class UrlViewSet(ModelViewSet):

    serializer_class = UrlSerializer
    queryset = Url.objects.all().order_by("-id")
    # queryset = Url.objects.filter(session_id=self.request.session.session_key).all()

    def get_queryset(self):
        queryset = (
            Url.objects.filter(session_id=self.request.session.session_key)
            .all()
            .order_by("-id")
        )
        return queryset

    @set_session
    def list(self, request):
        return super().list(request)

    @set_session
    def create(self, request):
        logging.debug('item was added')
        return super().create(request)

    # need to save user session to url table
    def perform_create(self, serializer):
        serializer.save(session_id=self.request.session.session_key)
