import os
from celery import Celery
import random
from celery.utils.log import get_task_logger


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")

app = Celery("app")
app.config_from_object("django.conf:settings", namespace="CELERY")

logger = get_task_logger(__name__)


@app.task(bind=True)
def remove_expired_urls(self):
    """remove expired urls from mysql and redis"""
    from django.db import connection
    from datetime import datetime, timedelta
    from django.conf import settings
    from short.models import Url

    logger.info("remove old urls")

    total = Url.objects.count()
    final_datetime = datetime.now() - timedelta(minutes=settings.URL_TTL)
    Url.objects.filter(сreated__lte=final_datetime).delete()

    after_delete = Url.objects.count()

    deleted = total - after_delete
    print(f"deleted {deleted} urls")
    return 1


# start send_messages each 10 sec
@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(10.0, remove_expired_urls.s(), name="every 10")
    logger.info("setup periodic tasks")


app.autodiscover_tasks()
