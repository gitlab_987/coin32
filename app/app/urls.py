from django.contrib import admin
from django.urls import path, include
from rest_framework.schemas import get_schema_view
from django.views.generic import TemplateView
from django.conf.urls import handler404, handler500, handler403, handler400


urlpatterns = [
    path(
        "swagger-ui/",
        TemplateView.as_view(
            template_name="/app/short/templates/swagger-ui.html",
            extra_context={"schema_url": "openapi-schema"},
        ),
        name="swagger-ui",
    ),
    path(
        "openapi",
        get_schema_view(
            title="Url Shortener", description="API for all things …", version="1.0.0"
        ),
        name="openapi-schema",
    ),
    # ...
    path(r"admin", admin.site.urls),
    path("", include("short.urls")),
]

handler404 = "short.views.page_not_found"
